#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <omp.h>
#include <string>
#include <cstring>

#define BOARD_SIZE 60

int currentBoard[BOARD_SIZE][BOARD_SIZE] = {0};
int previousBoard[BOARD_SIZE][BOARD_SIZE] = {0};

int counter = 0;

int countNeighbours(int posX, int posY);

void updateBoard(int posX, int posY);

void drawRandomBoard();

int main(int argc, char *argv[])
{

    int num_of_iterations = atoi(argv[1]);
    srand(time(NULL));

    drawRandomBoard();

    omp_set_num_threads(8);

    for (int i = 0; i < num_of_iterations; i++)
    {

#pragma omp for collapse(2)
        for (int row = 1; row < (BOARD_SIZE - 1); row++)
        {
            for (int col = 1; col < (BOARD_SIZE - 1); col++)
            {
                updateBoard(row, col);
            }
        }

        std::ofstream file;
        std::string filename;
        filename.append(std::to_string(counter));
        filename.append(".ppm");
        file.open(filename);

        // naglowek PPM
        file << "P3\n"
             << BOARD_SIZE << " " << BOARD_SIZE << "\n";
        file << 255 << "\n";
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                if (currentBoard[i][j] == 0)
                    file << " 0 0 0   ";
                else
                {
                    file << " 255 255 255   ";
                }
            }
            file << std::endl;
        }
        file.close();
        counter++;
        memcpy(previousBoard, currentBoard, sizeof(int) * BOARD_SIZE * BOARD_SIZE);
    }
}

void drawRandomBoard()
{
    // losowanie planszy
    for (int row = 1; row < (BOARD_SIZE - 1); row++)
    {
        for (int col = 1; col < (BOARD_SIZE - 1); col++)
        {
            // drawing numbers 1-10
            // if less than 3 = cell is alive
            // 20% for being alive
            if (((std::rand() % 10) + 1) < 2)
                previousBoard[row][col] = 1;
            else
                previousBoard[row][col] = 0;
        }
    }
}

int countNeighbours(int posX, int posY)
{
    int counter = 0;

    // jesli dany sasiad == 1, to jest, wiec dodajemy to liczby sasiadow
    // jesli sasiad == 0, czyli nie istnieje, to nie zmieni licznika
    counter += previousBoard[posX - 1][posY - 1];
    counter += previousBoard[posX - 1][posY];
    counter += previousBoard[posX - 1][posY + 1];
    counter += previousBoard[posX][posY - 1];
    counter += previousBoard[posX][posY + 1];
    counter += previousBoard[posX + 1][posY - 1];
    counter += previousBoard[posX + 1][posY];
    counter += previousBoard[posX + 1][posY + 1];

    return counter;
}

void updateBoard(int posX, int posY)
{
    int neighboursCount = countNeighbours(posX, posY);
    if (previousBoard[posX][posY] == 0)
    {
        if (neighboursCount == 3)
        {
            currentBoard[posX][posY] = 1;
        }

        else
        {
            currentBoard[posX][posY] = 0;
        }
    }

    else
    {
        if (neighboursCount == 2 || neighboursCount == 3)
        {
            currentBoard[posX][posY] = 1;
        }

        else
        {
            currentBoard[posX][posY] = 0;
        }
    }
}
